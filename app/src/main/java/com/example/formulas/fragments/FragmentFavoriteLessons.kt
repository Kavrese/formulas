package com.example.formulas.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.formulas.R
import com.example.formulas.common.ConnectController
import com.example.formulas.common.ConnectControllerAcceptPoint
import com.example.formulas.common.OnChangeListIdsLessons
import com.example.formulas.common.UserDataSession
import com.example.formulas.common.dialogs.DialogFactory
import com.example.formulas.models.ModelLesson
import kotlinx.android.synthetic.main.fragment_favorite_lessons.*

class FragmentFavoriteLessons: Fragment() {

    var data: MutableList<ModelLesson> = mutableListOf()

    private val onChangeListIdsLessons = object: OnChangeListIdsLessons{
        override fun onNewIdLesson(id: Int) {
            addLesson(id)
        }

        override fun onRemoveIdLesson(id: Int) {
            lessons_tags_favorite.removeLesson(id)
            data = lessons_tags_favorite.getData().toMutableList()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_favorite_lessons, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        UserDataSession.userSession.favoriteUser!!.addOnChangeListIdsLessons(onChangeListIdsLessons)

        getData()
    }

    private fun getData(){
        val listFavoritesIds = UserDataSession.userSession.favoriteUser!!.getAllListElements()
        listFavoritesIds.forEach {
            addLesson(it)
        }
    }

    private fun addLesson(idLesson: Int) {
        ConnectControllerAcceptPoint.connectController.requestGetLesson(idLesson.toString(), object: ConnectController.OnGetData<ModelLesson>{
            override fun onGet(data: ModelLesson) {
                lessons_tags_favorite.addData(data)
            }

            override fun onFail(message: String) {
                DialogFactory.createMessageDialogVariantTitle(requireContext(),
                    DialogFactory.TitleVariant.ERROR_REQUEST_LESSON,
                    message, DialogFactory.ButtonsVariant.OK_NULL
                ).show()
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        UserDataSession.userSession.favoriteUser?.removeOnChangeListIdsLessons(onChangeListIdsLessons)
    }
}