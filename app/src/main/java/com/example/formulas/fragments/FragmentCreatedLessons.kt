package com.example.formulas.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.formulas.R

class FragmentCreatedLessons: Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_created_lessons, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

    }
}