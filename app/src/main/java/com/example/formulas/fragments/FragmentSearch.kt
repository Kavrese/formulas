package com.example.formulas.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.formulas.R
import com.example.formulas.common.ConnectController
import com.example.formulas.common.ConnectControllerAcceptPoint
import com.example.formulas.common.Utils
import com.example.formulas.common.adapters.LessonsAdapter
import com.example.formulas.common.adapters.TagsAdapterSelectTag
import com.example.formulas.common.dialogs.DialogFactory
import com.example.formulas.models.ModelLesson
import com.example.formulas.screens.LessonActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.layout_bottom_sheet_search.*
import kotlinx.android.synthetic.main.layout_search.*

class FragmentSearch: Fragment() {

    private val categoryTag = listOf(
        "Алгебра", "Стереометрия", "Планеметрия",
        "Русский", "Физика", "Химия", "Английский",
        "История", "Обществознание", "Литература",
        "Информатика"
    )

    private var originData = listOf<ModelLesson>()
    private var nowSelectCategory: String? = null
    private var nowSelectTagsCategory = mutableListOf<String>()

    private lateinit var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        bottomSheetBehavior = BottomSheetBehavior.from(lin_bottom_sheet)

        rec_all_tags.apply {
            adapter = TagsAdapterSelectTag(TagsAdapterSelectTag.VariantItem.DEFAULT,
            variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.MULTI,
            onChangeSelect = object: TagsAdapterSelectTag.SelectTagsController.OnChangeSelectTags{
                override fun onChange(selectTags: List<String>) {
                    nowSelectTagsCategory.clear()
                    nowSelectTagsCategory.add(nowSelectCategory!!)
                    nowSelectTagsCategory.addAll(selectTags)
                    showHideButtonSearch()
                }
            })
            layoutManager = Utils.createFlexBoxManager(requireContext())
        }

        rec_category.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = TagsAdapterSelectTag(TagsAdapterSelectTag.VariantItem.BIG, categoryTag.toMutableList(),
                variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.ONLY_ONE,
                onChangeSelect = object: TagsAdapterSelectTag.SelectTagsController.OnChangeSelectTags{
                    override fun onChange(selectTags: List<String>) {
                        setSelectCategory(if (selectTags.isNotEmpty()) selectTags[0] else null)
                    }
                }
            )
        }

        rec_search.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = LessonsAdapter(onClickItem = object: LessonsAdapter.OnClickItem{
                override fun onClick(modelLesson: ModelLesson) {
                    val intent = Intent(context, LessonActivity::class.java)
                    intent.putExtra("id", modelLesson.id.toString())
                    requireActivity().startActivityForResult(intent, 105)
                }
            })
        }

        search_button.setOnClickListener {
            val searchText = text_search.text.toString()
            searchLessons(originData, searchText, nowSelectTagsCategory)
        }

        getData()
    }

    private fun searchLessons(originData: List<ModelLesson>, textSearch: String, tagsSearch: List<String>){
        var data = originData.filter { it.tagsList.containsAll(tagsSearch) }
        data = data.sortedByDescending {Utils.coefficientSearchTwoString(it.title, textSearch)}
        setSearchData(data)
    }

    private fun setSearchData(data: List<ModelLesson>){
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        (rec_search.adapter!! as LessonsAdapter).setData(data)
    }

    private fun showHideButtonSearch(){
        if (nowSelectCategory != null && nowSelectTagsCategory.isNotEmpty()) {
            search_button.visibility = View.VISIBLE
            scrollToBottom()
        }else
            search_button.visibility = View.GONE
    }

    private fun scrollToBottom(){
        scroll.post {
            // При screenOrientation="landscape" smoothScrollTo(0, scroll.height) работает не до конца ScrollView
            // Поэтому костыль в виде +100000 на ось y
            scroll.smoothScrollTo(0, scroll.height + 100000)
        }
    }

    private fun setSelectCategory(selectCategory: String?){
        nowSelectCategory = selectCategory

        if (selectCategory != null) {
            lin_search.visibility = View.VISIBLE
            all_selected_tags.text = "Все теги $selectCategory"

            nowSelectTagsCategory.clear()
            nowSelectTagsCategory.add(selectCategory)

            val allTagsCategory = Utils.convertLessonsToTags(
                originData.filter{selectCategory in it.tagsList}
            ).toMutableList()
            allTagsCategory.remove(selectCategory)

            (rec_all_tags.adapter!! as TagsAdapterSelectTag).setData(allTagsCategory)

            scrollToBottom()
        } else {
            nowSelectTagsCategory.clear()
            lin_search.visibility = View.GONE
        }

        showHideButtonSearch()
    }

    private fun getData(){
        ConnectControllerAcceptPoint.connectController.requestGetAllLessons(object: ConnectController.OnGetData<List<ModelLesson>>{
            override fun onGet(data: List<ModelLesson>) {
                originData = data
            }

            override fun onFail(message: String) {
                DialogFactory.createMessageDialogVariantTitle(
                    requireContext(), DialogFactory.TitleVariant.ERROR_REQUEST_LESSONS,
                    message, DialogFactory.ButtonsVariant.OK_NULL
                )
            }
        })
    }
}