package com.example.formulas.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.formulas.R
import com.example.formulas.common.UserDataSession
import com.example.formulas.common.adapters.FragmentPagerAdapter
import com.example.formulas.screens.AuthActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.layout_profile.*

class FragmentProfile: Fragment() {

    private val userData = UserDataSession.userSession.getUserData()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        nickname_profile.text = userData!!.nickname
        if (userData.avatar != null)
            Glide.with(requireContext())
                .load(userData.avatar)
                .into(avatar)

        exit.setOnClickListener {
            UserDataSession.userSession.exitUser()
            startActivity(Intent(requireContext(), AuthActivity::class.java))
            requireActivity().finish()
        }

        pager_profile.adapter = FragmentPagerAdapter(requireActivity(), listOf(
            FragmentCreatedLessons(),
            FragmentFavoriteLessons()
        ))
        pager_profile.isUserInputEnabled = false

        tabs.addOnTabSelectedListener(object: TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                // tab!!.id всегда равен -1
                when(tab!!.text){
                    "Созданные" -> {
                        pager_profile.setCurrentItem(0, false)
                    }
                    "Избранные" -> {
                        pager_profile.setCurrentItem(1, false)
                    }
                }
            }
            override fun onTabUnselected(tab: TabLayout.Tab?) { }
            override fun onTabReselected(tab: TabLayout.Tab?) {}
        })
    }
}