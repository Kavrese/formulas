package com.example.formulas.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.formulas.R
import com.example.formulas.common.ConnectController
import com.example.formulas.common.ConnectControllerAcceptPoint
import com.example.formulas.common.dialogs.DialogFactory
import com.example.formulas.models.ModelLesson
import com.example.formulas.screens.CreateEditLessonActivity
import kotlinx.android.synthetic.main.layout_main.*

class FragmentMain: Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.layout_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        add.setOnClickListener {
            val intent = Intent(requireContext(), CreateEditLessonActivity::class.java)
            intent.putExtra("isCreateNew", true)
            startActivityForResult(intent, 100)
        }
        refreshData()
    }

    fun refreshData(){
        ConnectControllerAcceptPoint.connectController.requestGetAllLessons(object: ConnectController.OnGetData<List<ModelLesson>>{
            override fun onGet(data: List<ModelLesson>) {
                lessons_tags.setData(data)
            }

            override fun onFail(message: String) {
                DialogFactory.createMessageDialogVariantTitle(
                    requireContext(), DialogFactory.TitleVariant.ERROR_REQUEST_LESSONS,
                    message, DialogFactory.ButtonsVariant.OK_NULL
                ).show()
            }
        })
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK){
            refreshData()
        }
    }
}