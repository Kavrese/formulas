package com.example.formulas.view

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import com.example.formulas.R
import kotlinx.android.synthetic.main.activity_lesson.*
import kotlinx.android.synthetic.main.layout_favorite_state_button_view.view.*

class FavoriteStateButton: LinearLayout {
    constructor(context: Context): super(context)
    constructor(context: Context, attr: AttributeSet): super(context, attr)
    constructor(context: Context, attr: AttributeSet, defStyle: Int): super(context, attr, defStyle)

    init {
        inflate(context, R.layout.layout_favorite_state_button_view, this)
    }

    var onChangeState: OnChangeState? = null
    private val states = listOf(State.NO_SELECT, State.SELECT)
    private var nowStateIndex = 0

    override fun onFinishInflate() {
        super.onFinishInflate()

        favorite_button_image.setOnClickListener {
            setNextState()
            onChangeState?.onChange(getNowState(), true)
        }
    }

    fun setState(state: State): Boolean{
        if (state !in states)
            return false
        setStateIndex(states.indexOf(state))
        onChangeState?.onChange(getNowState(), false)
        return true
    }

    private fun setNextState(){
        setStateIndex(nextIndexState())
    }

    private fun setStateIndex(index: Int){
        nowStateIndex = index
        val newState = getNowState()
        setStateButton(newState)
    }

    private fun setStateButton(state: State) {
        favorite_button_image.setImageResource(state.drawableId)
    }

    private fun nextIndexState(): Int = if (nowStateIndex != states.lastIndex) nowStateIndex + 1 else 0


    fun getNowState(): State = states[nowStateIndex]


    fun getStates(): List<State> = states

    interface OnChangeState {
        fun onChange(state: State, isUser: Boolean)
    }

    enum class State(val drawableId: Int){
        SELECT(R.drawable.ic_baseline_bookmark_24), NO_SELECT(R.drawable.ic_baseline_bookmark_border_24)
    }
}