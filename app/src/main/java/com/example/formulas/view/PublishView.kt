package com.example.formulas.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import com.example.formulas.R
import com.example.formulas.models.UserData
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.layout_publish_person_view.view.*
import kotlinx.android.synthetic.main.layout_publish_view.view.*
import kotlinx.android.synthetic.main.layout_publish_view.view.title_publish

class PublishView: LinearLayout {
    constructor(context: Context): super(context)
    constructor(context: Context, attr: AttributeSet): super(context, attr)
    constructor(context: Context, attr: AttributeSet, defStyle: Int): super(context, attr, defStyle)

    private var variantLayout: VariantView? = null
    var onClickButton: OnClickButton? = null
    var personData: UserData? = null

    fun setViewVariantLayout(variantLayout: VariantView){
        this.variantLayout = variantLayout
        removeAllViews()
        inflate(context, variantLayout.idLayout, this)
        refreshDataView()
    }

    fun refreshDataView(){
        rootView.title_publish.text = variantLayout!!.variantTitle.toString()
        when(variantLayout!!.idLayout){
            R.layout.layout_publish_person_view -> {
                if (personData != null)
                    setViewPersonData(personData!!)
            }
            R.layout.layout_publish_view -> {
                setButtons(variantLayout!!.variantButtons)
            }
        }
    }

    private fun setViewPersonData(userData: UserData){
        Glide.with(context)
            .load(userData.avatar)
            .into(rootView.avatar_person)
        rootView.name_person.text = userData.nickname
    }

    private fun setButtons(variantButtons: VariantsButtons){
        val isVisible = variantButtons != VariantsButtons.NONE
        rootView.lin_publish_buttons.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            val isTwo = variantButtons.secondButton != VariantButton.NONE
            setVisibleTwoButtons(isTwo)
            if (isTwo)
                setButton(rootView.publish_button_second, variantButtons.secondButton)
            setButton(rootView.publish_button_first, variantButtons.firstButton)
        }
    }

    private fun setButton(button: MaterialButton, variantButton: VariantButton){
        button.text = variantButton.toString()
        button.setOnClickListener { onClickButton?.onClick(variantButton)}
    }

    private fun setVisibleTwoButtons(isTwo: Boolean){
        val param = rootView.publish_button_first.layoutParams as LayoutParams
        param.weight = if (isTwo) 1.0f else 2.0f
        rootView.publish_button_first.layoutParams = param
        if (isTwo){
            rootView.publish_button_second.layoutParams = param
        }
    }

    enum class VariantView(
        val idLayout: Int,
        val variantTitle: VariantTitle,
        val variantButtons: VariantsButtons){

        EDIT_PUBLISH_YOU(R.layout.layout_publish_view, VariantTitle.PUBLISH_YOU, VariantsButtons.EDIT),
        PUBLISH_PERSON(R.layout.layout_publish_person_view, VariantTitle.PUBLISH, VariantsButtons.NONE),
        NOT_PUBLISH_YOU(R.layout.layout_publish_view, VariantTitle.NOT_PUBLISH, VariantsButtons.PUBLISH_OR_EDIT),
        NOT_APPROVE(R.layout.layout_publish_view, VariantTitle.NO_APPROVE, VariantsButtons.NONE),
        CHOOSE_APPROVE(R.layout.layout_publish_view, VariantTitle.NO_APPROVE, VariantsButtons.CHOOSE_APPROVE)
    }

    enum class VariantTitle(private val title: String){
        PUBLISH("Опубликованно"), PUBLISH_YOU("Опубликованно вами"), NOT_PUBLISH("Не опубликованно"),
        NO_APPROVE("Не одобренно");
        override fun toString() = title
    }

    enum class VariantsButtons(val firstButton: VariantButton, val secondButton: VariantButton){
        PUBLISH_OR_EDIT(VariantButton.PUBLISH, VariantButton.EDIT),
        CHOOSE_APPROVE(VariantButton.APPROVE, VariantButton.NOT_APPROVE),
        EDIT(VariantButton.EDIT, VariantButton.NONE),
        NONE(VariantButton.NONE, VariantButton.NONE)
    }

    enum class VariantButton(private val text: String){
        EDIT("Изменить"), PUBLISH("Опубликовать"), APPROVE("Одобрить"),
        NONE(""), NOT_APPROVE("Отказать");
        override fun toString() = text
    }

    interface OnClickButton {
        fun onClick(variantButton: VariantButton)
    }
}