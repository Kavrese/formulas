package com.example.formulas.view

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.util.Log
import android.widget.LinearLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.formulas.R
import com.example.formulas.common.Utils
import com.example.formulas.common.adapters.LessonsAdapter
import com.example.formulas.common.adapters.TagsAdapterSelectTag
import com.example.formulas.models.ModelLesson
import com.example.formulas.screens.LessonActivity
import kotlinx.android.synthetic.main.layout_lessons_tag.view.*
import java.lang.IndexOutOfBoundsException

class LessonsTagView: LinearLayout {
    constructor(context: Context): super(context)
    constructor(context: Context, attr: AttributeSet): super(context, attr)
    constructor(context: Context, attr: AttributeSet, defStyle: Int): super(context, attr, defStyle)

    private var originData: MutableList<ModelLesson> = mutableListOf()

    init {
        inflate(context, R.layout.layout_lessons_tag, this)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        rootView.rec_data.apply {
            adapter = LessonsAdapter(onClickItem = object: LessonsAdapter.OnClickItem{
                override fun onClick(modelLesson: ModelLesson) {
                    val intent = Intent(context, LessonActivity::class.java)
                    intent.putExtra("id", modelLesson.id.toString())
                    try {
                        (context as Activity).startActivityForResult(intent, 100)
                    }catch (e: Exception){
                        context.startActivity(intent)
                    }
                }
            })
            layoutManager = CustomStaggerGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        }

        rootView.rec_tags.apply {
            adapter = TagsAdapterSelectTag(
                TagsAdapterSelectTag.VariantItem.DEFAULT,
                variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.MULTI,
                onChangeSelect = object: TagsAdapterSelectTag.SelectTagsController.OnChangeSelectTags {
                    override fun onChange(selectTags: List<String>) {
                        (rootView.rec_data.adapter!! as LessonsAdapter).setData(originData.filter {
                            it.tagsList.containsAll(
                                selectTags
                            )
                        })
                    }
                }
            )
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }

    fun setData(data: List<ModelLesson>){
        (rec_data.adapter!! as LessonsAdapter).setData(data)
        (rec_tags.adapter!! as TagsAdapterSelectTag).setData(Utils.convertLessonsToTags(data))
        originData = data.toMutableList()
    }

    fun addData(data: ModelLesson){
        (rec_data.adapter!! as LessonsAdapter).addData(data)
        (rec_tags.adapter!! as TagsAdapterSelectTag).addData(data.tagsList)
        originData.add(data)
    }

    fun clearData(){
        (rec_data.adapter!! as LessonsAdapter).clearData()
        (rec_tags.adapter!! as TagsAdapterSelectTag).clearData()
        originData.clear()
    }

    fun getData(): List<ModelLesson> = originData.toList()

    fun removeLesson(idLessons: Int){
        try {
            originData.remove(originData.filter { it.id == idLessons }[0])
            setData(originData)
        }catch (e: IndexOutOfBoundsException){}
    }
}

class CustomStaggerGridLayoutManager(spanCount: Int, orientation: Int) :
    StaggeredGridLayoutManager(spanCount, orientation) {

    override fun onScrollStateChanged(state: Int) {
        try {
            super.onScrollStateChanged(state)
        }catch (e: IndexOutOfBoundsException){
            Log.e("custom-stagger-grid-layout-manager", e.stackTraceToString())
        }
    }
    
}