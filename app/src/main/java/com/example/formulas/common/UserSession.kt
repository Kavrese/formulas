package com.example.formulas.common

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import com.example.formulas.models.UserData
import com.example.formulas.view.FavoriteStateButton

object UserDataSession{
   lateinit var userSession: UserSession
}

class UserSession {

    private var defaultSh: SharedPreferences
    private var userSh: SharedPreferences? = null
    private var userData: UserData? = null
    var favoriteUser: FavoriteUser? = null

    constructor(activity: Activity): this(activity.applicationContext)
    private constructor(context: Context){
        defaultSh = createSharedPreference(context, "default")
        val lastUserNickname = getLastEnterNickname()
        if (lastUserNickname != null){
            enterUser(context, lastUserNickname)
        }
    }

    fun enterUser(context: Context, nickname: String){
        createUserSh(context, nickname)
        favoriteUser = FavoriteUser(userSh!!)
        if (!nicknameExist(nickname))
            setUserDataMemory(nickname)
        saveLastEnterNickname(nickname)
        userData = getUserData()
    }

    fun exitUser(){
        favoriteUser?.removeAllOnChangeListIdsLessons()
        userSh = null
        favoriteUser = null
        userData = null
        saveLastEnterNickname(null)
    }

    fun getUserData(): UserData?{
        return userData ?: if (userSh != null) getUserDataFromMemory() else null
    }

    private fun setNicknameExist(nickname: String){
        defaultSh.edit()
            .putBoolean("exist-$nickname", true)
            .apply()
    }

    private fun nicknameExist(nickname: String): Boolean{
        return defaultSh.getBoolean("exist-$nickname", false)
    }

    private fun createUserSh(context: Context, userNickname: String){
        userSh = createSharedPreference(context, userNickname)
    }

    private fun createSharedPreference(context: Context, name: String): SharedPreferences{
        return context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    fun saveUserData(userData: UserData){
        this.userData = userData
        setNicknameExist(userData.nickname)
        userSh!!.edit()
            .putString("nickname", userData.nickname)
            .apply()
    }

    private fun setUserDataMemory(nickname: String){
        userSh!!.edit().putString("nickname", nickname).apply()
    }

    private fun getUserDataFromMemory(): UserData{
        return UserData(
            userSh!!.getString("nickname", "")!!
        )
    }

    private fun saveLastEnterNickname(nickname: String?){
        defaultSh.edit()
            .putString("last-enter", nickname)
            .apply()
    }

    private fun getLastEnterNickname(): String? = defaultSh.getString("last-enter", null)

    fun removeUserData(){
        userData = null
        userSh!!.edit()
            .remove("nickname")
            .apply()
    }

    fun saveStateFavoriteIdLesson(idLesson: Int, state: FavoriteStateButton.State){
        when(state){
            FavoriteStateButton.State.SELECT -> {
                UserDataSession.userSession.favoriteUser!!.saveObject(idLesson)
            }
            FavoriteStateButton.State.NO_SELECT -> {
                UserDataSession.userSession.favoriteUser!!.removeObject(idLesson)
            }
        }
    }

    class FavoriteUser(private val userSh: SharedPreferences): MemoryListDataController{

        enum class Tags(val tag: String){
            LIST_ITEM("favorite-list-"), OBJECT("favorite-"), SIZE_LIST_ITEMS("favorite-list-size");

            override fun toString(): String = tag
        }

        private val listOnChangeListIdsLessons: MutableList<OnChangeListIdsLessons> = mutableListOf()

        override fun removeListElement(id: Int) {
            val listFavorite = getAllListElements().toMutableList()
            if (id in listFavorite) {
                clearAllListElements()
                listFavorite.remove(id)
                saveAllListElements(listFavorite)
            }
        }

        override fun saveAllListElements(ids: List<Int>) {
            ids.forEach { saveListElement(it) }
        }

        override fun saveListElement(id: Int) {
            val newIndex = userSh.getInt(Tags.SIZE_LIST_ITEMS.tag, 0)
            userSh.edit()
                .putInt("${Tags.LIST_ITEM}$newIndex", id)
                .putInt(Tags.SIZE_LIST_ITEMS.tag, newIndex + 1)
                .apply()
        }

        override fun getAllListElements(): List<Int> {
            val size = userSh.getInt(Tags.SIZE_LIST_ITEMS.tag, 0)
            return (0 until size).map { userSh.getInt("${Tags.LIST_ITEM}$it", -1) }
        }

        override fun clearAllListElements() {
            val size = userSh.getInt(Tags.SIZE_LIST_ITEMS.tag, 0)
            (0..size).forEach {
                userSh.edit().remove("${Tags.LIST_ITEM}$it").apply()
            }
            userSh.edit().remove(Tags.SIZE_LIST_ITEMS.tag).apply()
        }

        override fun isExistElementList(id: Int): Boolean {
            return id in getAllListElements()
        }

        override fun saveObject(id: Int) {
            userSh.edit()
                .putBoolean("${Tags.OBJECT}$id", true)
                .apply()
            saveListElement(id)
            listOnChangeListIdsLessons.forEach{ it.onNewIdLesson(id) }
        }

        override fun removeObject(id: Int) {
            userSh.edit()
                .remove("${Tags.OBJECT}$id")
                .apply()
            removeListElement(id)
            listOnChangeListIdsLessons.forEach{ it.onRemoveIdLesson(id) }
        }

        override fun isExistObject(id: Int): Boolean {
            return userSh.getBoolean("${Tags.OBJECT}$id", false)
        }

        fun addOnChangeListIdsLessons(onChangeListIdsLessons: OnChangeListIdsLessons){
            listOnChangeListIdsLessons.add(onChangeListIdsLessons)
        }

        fun removeAllOnChangeListIdsLessons(){
            listOnChangeListIdsLessons.forEach{removeOnChangeListIdsLessons(it)}
        }

        fun removeOnChangeListIdsLessons(onChangeListIdsLessons: OnChangeListIdsLessons){
            listOnChangeListIdsLessons.remove(onChangeListIdsLessons)
        }
    }
}

