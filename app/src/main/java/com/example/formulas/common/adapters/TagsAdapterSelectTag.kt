package com.example.formulas.common.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.formulas.R
import kotlinx.android.synthetic.main.item_tag.view.*

class SimpleViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

class TagsAdapterSelectTag(
    private var variantItem: VariantItem,
    val tags: MutableList<String> = mutableListOf(),
    var onClickTag: OnClickTag?=null,
    variantSelect: SelectTagsController.VariantSelect = SelectTagsController.VariantSelect.NONE,
    onChangeSelect: SelectTagsController.OnChangeSelectTags? = null
): RecyclerView.Adapter<SimpleViewHolder>() {

    private val selectTagsController =
        if (variantSelect != SelectTagsController.VariantSelect.NONE)
            SelectTagsController(variantSelect, onChangeSelect, this)
        else null

    interface OnClickTag{
        fun onClick(tag: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(variantItem.id, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        val variantItem = selectTagsController?.getVariantItem(tags[position]) ?: VariantStyleItem.DEFAULT
        val context = holder.itemView.context
        holder.itemView.tag_item.setTextColor(ContextCompat.getColor(context, variantItem.colorText))
        holder.itemView.tag_item.setBackgroundColor(ContextCompat.getColor(context, variantItem.colorBackground))

        holder.itemView.tag_item.text = tags[position]
        holder.itemView.setOnClickListener {
            onClickTag?.onClick(tags[position])
            selectTag(position)
        }
    }

    override fun getItemCount(): Int = tags.size

    fun setData(tags: List<String>){
        clearData()
        this.tags.addAll(tags)
        notifyDataSetChanged()
    }

    fun addData(data: List<String>){
        this.tags.addAll(data.filter { it !in tags })
        notifyDataSetChanged()
    }

    fun addDataElement(data: String){
        this.tags.add(data)
        notifyDataSetChanged()
    }

    fun clearData(){
        tags.clear()
        selectTagsController?.removeAllTags()
        notifyDataSetChanged()
    }

    fun selectTag(position: Int){
        selectTagsController?.onClickTag(tags[position])
    }

    enum class VariantStyleItem(val colorText: Int, val colorBackground: Int){
        DEFAULT(R.color.colorText, R.color.colorBackground), SELECT(R.color.colorBackground, R.color.colorText)
    }

    enum class VariantItem(val id: Int){
        SMALL(R.layout.item_tag_small), DEFAULT(R.layout.item_tag), BIG(R.layout.item_tag_big),
        SCREEN(R.layout.item_tag_screen)
    }

    class SelectTagsController(private val variantSelect: VariantSelect,
                               private val onChangeSelect: OnChangeSelectTags?,
                               private val adapterSelectTag: TagsAdapterSelectTag
    ){
        private val selectTags: MutableList<String> = mutableListOf()

        fun onClickTag(tag: String){
            if (tag !in selectTags) {
                if (variantSelect == VariantSelect.ONLY_ONE && selectTags.isNotEmpty())
                    removeAllTags()
                addSelectTag(tag)
            }else {
                removeSelectTag(tag)
            }
            onChangeSelect?.onChange(selectTags)
        }

        fun getVariantItem(tag: String): VariantStyleItem{
            return if (tag in selectTags) VariantStyleItem.SELECT else VariantStyleItem.DEFAULT
        }

        private fun addSelectTag(newSelectTag: String){
            selectTags.add(newSelectTag)
            notifyDataSetChanged()
        }

        private fun removeSelectTag(selectTag: String){
            selectTags.remove(selectTag)
            notifyDataSetChanged()
        }

        fun removeAllTags(){
            selectTags.clear()
            notifyDataSetChanged()
        }

        private fun notifyDataSetChanged(){
            adapterSelectTag.notifyDataSetChanged()
        }

        fun getSelectTags(): List<String> = selectTags

        interface OnChangeSelectTags{
            fun onChange(selectTags: List<String>)
        }

        enum class VariantSelect(){
            ONLY_ONE, MULTI, NONE
        }
    }
}