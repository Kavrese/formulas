package com.example.formulas.common

import com.example.formulas.models.ModelLesson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

object ConnectControllerAcceptPoint {
    val connectController = ConnectController()
}

class ConnectController {

    interface OnGetData<T>{
        fun onGet(data: T)
        fun onFail(message: String)
    }

    interface API {
        @GET("items.json")
        fun items(): Call<List<ModelLesson>>

        @GET("items/{id}.json")
        fun lesson(@Path("id") id: String): Call<ModelLesson>

        @PUT("items/{id}.json")
        fun saveModelLesson(@Path("id") id: String, @Body body: ModelLesson): Call<ModelLesson>
    }

    private val retrofit = initRetrofit("https://tasksworldskills-default-rtdb.firebaseio.com/Formulas/").create(API::class.java)

    companion object {
        fun initRetrofit(baseUrl: String): Retrofit {
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }

    fun requestGetAllLessons(onGetData: OnGetData<List<ModelLesson>>){
        retrofit.items().enqueue(object: Callback<List<ModelLesson>>{
            override fun onResponse(
                call: Call<List<ModelLesson>>,
                response: Response<List<ModelLesson>>
            ) {
                if (response.isSuccessful && response.body() != null){
                    response.body()!!.forEachIndexed{ index, _ -> response.body()!![index].id = index}
                    onGetData.onGet(response.body()!!)
                }else{
                    onGetData.onFail("${response.code()}, ${response.errorBody()}")
                }
            }

            override fun onFailure(call: Call<List<ModelLesson>>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }
        })
    }

    fun requestGetLesson(id: String, onGetData: OnGetData<ModelLesson>){
        retrofit.lesson(id).enqueue(object: Callback<ModelLesson>{
            override fun onResponse(
                call: Call<ModelLesson>,
                response: Response<ModelLesson>
            ) {
                if (response.isSuccessful && response.body() != null){
                    response.body()!!.id = id.toInt()
                    onGetData.onGet(response.body()!!)
                }else{
                    onGetData.onFail("${response.code()}, ${response.errorBody()}")
                }
            }

            override fun onFailure(call: Call<ModelLesson>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }
        })
    }

    fun requestSaveModelLesson(id: String, modelLesson: ModelLesson, onGetData: OnGetData<ModelLesson>){
        retrofit.saveModelLesson(id, modelLesson).enqueue(object: Callback<ModelLesson>{
            override fun onResponse(call: Call<ModelLesson>, response: Response<ModelLesson>) {
                if (response.isSuccessful && response.body() != null){
                    response.body()!!.id = id.toInt()
                    onGetData.onGet(response.body()!!)
                }else{
                    onGetData.onFail("${response.code()}, ${response.errorBody()}")
                }
            }

            override fun onFailure(call: Call<ModelLesson>, t: Throwable) {
                onGetData.onFail(t.message!!)
            }
        })
    }

    fun saveModelLesson(modelLesson: ModelLesson, onGetData: OnGetData<ModelLesson>){
        fun requestSaveModelLesson(id: String, modelLesson: ModelLesson, onGetData: OnGetData<ModelLesson>){
            this.requestSaveModelLesson(id, modelLesson, object: OnGetData<ModelLesson>{
                override fun onGet(data: ModelLesson) {
                    data.id = id.toInt()
                    onGetData.onGet(data)
                }
                override fun onFail(message: String) {
                    onGetData.onFail(message)
                }
            })
        }

        if (modelLesson.id == null)
            requestGetAllLessons(object: OnGetData<List<ModelLesson>>{
                override fun onGet(data: List<ModelLesson>) {
                    val indexNewModelLesson = data.size.toString()
                    requestSaveModelLesson(indexNewModelLesson, modelLesson, onGetData)
                }
                override fun onFail(message: String) {
                    onGetData.onFail(message)
                }
            })
        else
            requestSaveModelLesson(modelLesson.id!!.toString(), modelLesson, onGetData)
    }
}