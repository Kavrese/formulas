package com.example.formulas.common.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.formulas.R
import kotlinx.android.synthetic.main.item_more.view.*
import kotlinx.android.synthetic.main.item_more.view.title_more
import kotlinx.android.synthetic.main.item_more_screen_remove.view.*

class MoreAdapter(private var more: MutableList<String> = mutableListOf(),
                  val variantItem: VariantItem = VariantItem.DEFAULT): RecyclerView.Adapter<SimpleViewHolder>() {

    interface OnClickMore{
        fun onClick(more: String, index: Int)
    }

    var onClickMore: OnClickMore? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(variantItem.id, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        holder.itemView.title_more.text = more[position]
        if (variantItem == VariantItem.SCREEN_REMOVE){
            holder.itemView.remove_more.setOnClickListener {
                onClickMore?.onClick(more[position], position)
            }
        }
    }

    override fun getItemCount(): Int = more.size

    fun setData(data: List<String>){
        more.clear()
        more.addAll(data)
        notifyDataSetChanged()
    }

    fun getData(): List<String> {
        return more
    }

    fun clearData(){
        more.clear()
        notifyDataSetChanged()
    }

    fun removeDataPosition(position: Int){
        more.removeAt(position)
        notifyDataSetChanged()
    }

    fun addData(data: String){
        more.add(data)
        notifyDataSetChanged()
    }

    enum class VariantItem(val id: Int){
        DEFAULT(R.layout.item_more), SCREEN(R.layout.item_more_screen), SCREEN_REMOVE(R.layout.item_more_screen_remove)
    }
}