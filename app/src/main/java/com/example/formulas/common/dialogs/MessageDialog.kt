package com.example.formulas.common.dialogs

import android.content.Context
import android.os.Bundle
import android.view.View
import com.example.formulas.R
import kotlinx.android.synthetic.main.layout_message_dialog.*


class MessageDialog(context: Context, themeId: Int = R.style.AlertDialogTheme):
    PlatformDialog(context, themeId) {

    var title = ""
    var positiveText = ""
    var message = ""
    var negativeText = ""
    var onClickPositive: View.OnClickListener? = null
    var onClickNegative: View.OnClickListener? = null

    init {
        setCancelable(true)
    }

    class Builder(context: Context){
        private val dialog = MessageDialog(context)

        fun setTitle(title: String) = apply { dialog.title = title}
        fun setMessage(message: String) = apply {dialog.message = message}
        fun setOnClickPositive(textButton: String, onClickListener: View.OnClickListener?) = apply {
            dialog.positiveText = textButton
            dialog.onClickPositive = onClickListener
        }
        fun setOnClickNegative(textButton: String, onClickListener: View.OnClickListener?) = apply {
            dialog.onClickNegative = onClickListener
            dialog.negativeText = textButton
        }

        fun create(): MessageDialog{
            dialog.create()
            return dialog
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_message_dialog)

        title_dialog.text = title
        message_dialog.text = message

        if (negativeText.isNotEmpty()) {
            button_2_dialog_message.text = negativeText
            button_2_dialog_message.setOnClickListener(onClickNegative ?: View.OnClickListener {
                dismiss()
            })
        }else{
            button_2_dialog_message.visibility = View.GONE
        }

        if (positiveText.isNotEmpty()) {
            button_add.text = positiveText
            button_add.setOnClickListener(onClickPositive ?: View.OnClickListener {
                dismiss()
            })
        }else{
            button_add.visibility = View.GONE
        }
    }
}