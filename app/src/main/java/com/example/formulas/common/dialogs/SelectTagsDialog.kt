package com.example.formulas.common.dialogs

import android.content.Context
import android.os.Bundle
import com.example.formulas.R
import com.example.formulas.common.Utils
import com.example.formulas.common.adapters.TagsAdapterSelectTag
import kotlinx.android.synthetic.main.layout_select_tags_dialog.*
import java.lang.Exception

class SelectTagsDialog(context: Context,
                       themeId: Int = R.style.AlertDialogTheme,
                       private val tags: MutableList<String> = mutableListOf(),
                       private val alreadySelectTags: MutableList<String> = mutableListOf(),
                       private val onChangeSelectTags: TagsAdapterSelectTag.SelectTagsController.OnChangeSelectTags
                       ): PlatformDialog(context, themeId)  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_select_tags_dialog)

        close_dialog.setOnClickListener {
            dismiss()
        }

        rec_tags_dialog.apply {
            adapter = TagsAdapterSelectTag(
                variantItem = TagsAdapterSelectTag.VariantItem.DEFAULT,
                variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.MULTI,
                tags = tags,
                onChangeSelect = onChangeSelectTags
            )
            layoutManager = Utils.createFlexBoxManager(context)
        }

        alreadySelectTags.forEach {
            try {
                val position = tags.indexOf(it)
                (rec_tags_dialog.adapter!! as TagsAdapterSelectTag).selectTag(position)
            }catch (e: Exception){}
        }

        add_tag.setOnClickListener {
            DialogFactory.createPlainTextDialog(context, DialogFactory.TitleVariant.INPUT_NEW_TAG,
            DialogFactory.HintVariant.NEW_TAG, object: PlainTextDialog.OnResultEditTextDialog {
                    override fun onAdd(text: String) {
                        tags.add(0, text)
                        rec_tags_dialog.adapter!!.notifyDataSetChanged()
                    }

                    override fun onCancel() {}
                }).show()
        }
    }
}