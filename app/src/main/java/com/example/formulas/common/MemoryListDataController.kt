package com.example.formulas.common

interface MemoryListDataController{
    fun removeListElement(id: Int)
    fun saveAllListElements(ids: List<Int>)
    fun saveListElement(id: Int)
    fun getAllListElements(): List<Int>
    fun clearAllListElements()
    fun isExistElementList(id: Int): Boolean

    fun saveObject(id: Int)
    fun removeObject(id: Int)
    fun isExistObject(id: Int): Boolean
}

interface OnChangeListIdsLessons{
    fun onNewIdLesson(id: Int)
    fun onRemoveIdLesson(id: Int)
}