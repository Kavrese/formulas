package com.example.formulas.common.dialogs

import android.content.Context
import android.view.View

class DialogFactory {
    enum class TitleVariant(val text: String){
        ERROR_EDITTEXT("Ошибка заполения"),
        ERROR_REQUEST_LESSONS("Ошибка запроса получения теорем"),
        ERROR_REQUEST_LESSON("Ошибка запроса получения теоремы"),
        INPUT_MORE("Введите следствие"),
        INPUT_NEW_TAG("Введите новый тэг"),
        ERROR_SAVE("Ошибка сохранения")
    }

    enum class MessageVariant(val text: String){
        ERROR_NICKNAME_EDITTEXT("Заполните поле Nickname"),
        ERROR_GET_ID_LESSON("Не удалось получить id теоремы"),
        ERROR_SELECT_ONE_TAG("Выберите хотя бы один тег"),
        ERROR_TITLE("Заполните заголовок"),
        ERROR_DESC("Заполните описсание")
    }

    enum class HintVariant(val text: String){
        NEW_TAG("Новый тэг"),
        MORE("Следствие"),
    }

    enum class ButtonsVariant(val textPos: String, val onClickPos: View.OnClickListener?,
                              val textNeg: String, val onClickNeg: View.OnClickListener?){
        OK_NULL("OK", null, "", null)
    }

    companion object {
        fun createMessageDialogVariant(
            context: Context,
            titleVariant: TitleVariant,
            messageVariant: MessageVariant,
            buttonsVariant: ButtonsVariant
        ): MessageDialog {
            return MessageDialog.Builder(context)
                .setTitle(titleVariant.text)
                .setMessage(messageVariant.text)
                .setOnClickPositive(buttonsVariant.textPos, buttonsVariant.onClickPos)
                .setOnClickNegative(buttonsVariant.textNeg, buttonsVariant.onClickNeg)
                .create()
        }

        fun createMessageDialogVariantTitle(
            context: Context,
            titleVariant: TitleVariant,
            message: String,
            buttonsVariant: ButtonsVariant
        ): MessageDialog{
            return MessageDialog.Builder(context)
                .setTitle(titleVariant.text)
                .setMessage(message)
                .setOnClickPositive(buttonsVariant.textPos, buttonsVariant.onClickPos)
                .setOnClickNegative(buttonsVariant.textNeg, buttonsVariant.onClickNeg)
                .create()
        }

        fun createPlainTextDialog(context: Context, titleVariant: TitleVariant, hintVariant: HintVariant,
                                  onResultEditTextDialog: PlainTextDialog.OnResultEditTextDialog): PlainTextDialog{
            return PlainTextDialog.Builder(context)
                .setTitle(titleVariant.text)
                .setHintPlainText(hintVariant.text)
                .setOnResultEditTextDialog(onResultEditTextDialog)
                .create()
        }
    }
}