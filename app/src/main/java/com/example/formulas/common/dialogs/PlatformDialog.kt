package com.example.formulas.common.dialogs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.example.formulas.R

open class PlatformDialog(context: Context, themeId: Int = R.style.AlertDialogTheme): Dialog(context, themeId) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.decorView.setBackgroundResource(android.R.color.transparent)
        setCancelable(true)
    }
}