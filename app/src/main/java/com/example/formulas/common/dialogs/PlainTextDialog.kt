package com.example.formulas.common.dialogs

import android.content.Context
import android.os.Bundle
import com.example.formulas.R
import kotlinx.android.synthetic.main.layout_edittext_more_dialog.*
import kotlinx.android.synthetic.main.layout_message_dialog.button_add

class PlainTextDialog(context: Context): PlatformDialog(context) {

    interface OnResultEditTextDialog{
        fun onAdd(text: String)
        fun onCancel()
    }

    var title = ""
    var hintEditText = ""
    var onResultEditTextDialog: OnResultEditTextDialog? = null

    class Builder(context: Context) {
        private val plainTextDialog = PlainTextDialog(context)

        fun setTitle(title: String): Builder = apply { plainTextDialog.title = title }
        fun setHintPlainText(hint: String): Builder = apply { plainTextDialog.hintEditText = hint }
        fun setOnResultEditTextDialog(onResultEditTextDialog: OnResultEditTextDialog): Builder =
            apply { plainTextDialog.onResultEditTextDialog = onResultEditTextDialog }

        fun create(): PlainTextDialog = plainTextDialog
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.layout_edittext_more_dialog)
        setCancelable(false)

        title_dialog_add.text = title
        dialog_edittext.hint = hintEditText

        button_add.setOnClickListener {
            val newMore = dialog_edittext.text.toString()
            onResultEditTextDialog?.onAdd(newMore)
            dismiss()
        }

        button_cancel.setOnClickListener {
            onResultEditTextDialog?.onCancel()
            dismiss()
        }
    }
}