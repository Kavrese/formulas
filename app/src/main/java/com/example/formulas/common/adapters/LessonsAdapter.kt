package com.example.formulas.common.adapters

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.formulas.R
import com.example.formulas.models.ModelLesson
import com.example.formulas.screens.LessonActivity
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayout
import com.google.android.flexbox.FlexboxLayoutManager
import kotlinx.android.synthetic.main.item_lesson.view.*

class LessonsAdapter(private val data: MutableList<ModelLesson> = mutableListOf(),
                     private val onClickItem: OnClickItem? = null,
                     private val toLessonScreen: Boolean = true
): RecyclerView.Adapter<SimpleViewHolder>() {

    interface OnClickItem{
        fun onClick(modelLesson: ModelLesson)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleViewHolder {
        return SimpleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_lesson, parent, false))
    }

    override fun onBindViewHolder(holder: SimpleViewHolder, position: Int) {
        val context = holder.itemView.context
        holder.itemView.title_lesson.text = data[position].title
        holder.itemView.desc_lesson.text = data[position].text
        holder.itemView.setOnClickListener {
            onClickItem?.onClick(data[position])
            if (toLessonScreen) {

            }
        }

        holder.itemView.rec_tags_lesson_item.apply {
            adapter = TagsAdapterSelectTag(TagsAdapterSelectTag.VariantItem.SMALL, data[position].tagsList.toMutableList())
            layoutManager = LinearLayoutManager(holder.itemView.context, LinearLayoutManager.HORIZONTAL, false)
        }

        if (data[position].moreList != null){
            holder.itemView.rec_more.apply{
                adapter = MoreAdapter(data[position].moreList!!.toMutableList())
                layoutManager = LinearLayoutManager(holder.itemView.context)
            }
            holder.itemView.lin_more.visibility = View.VISIBLE
        }else{
            holder.itemView.lin_more.visibility = View.GONE
        }
    }

    override fun getItemCount(): Int = data.size

    fun setData(data: List<ModelLesson>){
        this.data.clear()
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun addData(data: ModelLesson){
        this.data.add(data)
        notifyDataSetChanged()
    }

    fun clearData(){
        notifyDataSetChanged()
        data.clear()
    }
}