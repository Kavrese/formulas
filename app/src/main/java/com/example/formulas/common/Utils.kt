package com.example.formulas.common

import android.content.Context
import com.example.formulas.models.ModelLesson
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayoutManager

class Utils {
    companion object{
        fun convertLessonsToTags(lessons: List<ModelLesson>): List<String>{
            var tags = mutableListOf<String>()
            lessons.forEach { tags.addAll(it.tagsList) }
            tags = tags.toSet().toMutableList()
            return tags
        }

        fun coefficientSearchTwoString(first: String, second: String): Int {
            val wordsFirst = first.split(" ")
            val wordsSecond = second.split(" ")
            return wordsFirst.sumOf { if (it in wordsSecond) 1 as Int else 0 }
        }

        fun createFlexBoxManager(context: Context): FlexboxLayoutManager{
            val flexboxLayoutManager = FlexboxLayoutManager(context)
            flexboxLayoutManager.flexWrap = FlexWrap.WRAP
            flexboxLayoutManager.flexDirection = FlexDirection.ROW
            return flexboxLayoutManager
        }
    }
}