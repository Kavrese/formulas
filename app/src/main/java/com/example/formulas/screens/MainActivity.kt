package com.example.formulas.screens

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.view.children
import androidx.viewpager2.widget.ViewPager2
import com.example.formulas.R
import com.example.formulas.common.adapters.FragmentPagerAdapter
import com.example.formulas.fragments.FragmentMain
import com.example.formulas.fragments.FragmentProfile
import com.example.formulas.fragments.FragmentSearch
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val fragments = listOf(
        FragmentMain(),
        FragmentSearch(),
        FragmentProfile()
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        pager.adapter = FragmentPagerAdapter(this, fragments)

        bottom_nav_view.setOnItemSelectedListener {
            val position = bottom_nav_view.menu.children.indexOf(it)
            pager.setCurrentItem(position, true)
            return@setOnItemSelectedListener true
        }

        pager.registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                bottom_nav_view.selectedItemId = bottom_nav_view.menu.getItem(position).itemId
            }
        })

        pager.isUserInputEnabled = false
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100 && resultCode == Activity.RESULT_OK){
            (fragments[0] as FragmentMain).refreshData()
        }
    }
}