package com.example.formulas.screens

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.formulas.R
import com.example.formulas.common.ConnectController
import com.example.formulas.common.ConnectControllerAcceptPoint
import com.example.formulas.common.Utils
import com.example.formulas.common.adapters.MoreAdapter
import com.example.formulas.common.adapters.TagsAdapterSelectTag
import com.example.formulas.common.dialogs.PlainTextDialog
import com.example.formulas.common.dialogs.DialogFactory
import com.example.formulas.common.dialogs.SelectTagsDialog
import com.example.formulas.models.ModelLesson
import kotlinx.android.synthetic.main.activity_create_edit_lesson.*

class CreateEditLessonActivity : AppCompatActivity() {

    private var isCreateNew = true
    private var idLesson: Int? = null
    private var allTags: MutableList<String> = mutableListOf()

    private val onChangeSelectTags = object: TagsAdapterSelectTag.SelectTagsController.OnChangeSelectTags {
        override fun onChange(selectTags: List<String>) {
            (rec_tags_create_edit_lesson.adapter!! as TagsAdapterSelectTag).setData(selectTags)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_edit_lesson)

        isCreateNew = intent.extras?.getBoolean("isCreateNew") ?: true
        if (!isCreateNew){
            idLesson = intent.extras?.getInt("idLesson")
            if (idLesson == null)
                DialogFactory.createMessageDialogVariant( this,
                    DialogFactory.TitleVariant.ERROR_REQUEST_LESSON,
                    DialogFactory.MessageVariant.ERROR_GET_ID_LESSON,
                    DialogFactory.ButtonsVariant.OK_NULL
                ).show()
            else
                getData(idLesson.toString())
        }

        rec_tags_create_edit_lesson.apply {
            adapter = TagsAdapterSelectTag(
                variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.NONE,
                variantItem = TagsAdapterSelectTag.VariantItem.DEFAULT,
                onClickTag = object: TagsAdapterSelectTag.OnClickTag {
                    override fun onClick(tag: String) {
                        val tags = getTagsFromRec()
                        tags.remove(tag)
                        (rec_tags_create_edit_lesson.adapter!! as TagsAdapterSelectTag).setData(tags)
                    }
                }
            )
            layoutManager = Utils.createFlexBoxManager(this@CreateEditLessonActivity)
        }

        rec_create_edit_more.apply {
            adapter = MoreAdapter(variantItem = MoreAdapter.VariantItem.SCREEN_REMOVE)
            layoutManager = LinearLayoutManager(this@CreateEditLessonActivity)
        }
        (rec_create_edit_more.adapter!! as MoreAdapter).onClickMore = object: MoreAdapter.OnClickMore{
            override fun onClick(more: String, index: Int) {
                (rec_create_edit_more.adapter!! as MoreAdapter).removeDataPosition(index)
            }
        }

        back_create_edit.setOnClickListener {
            finish()
        }

        text_add_tag.setOnClickListener {
            SelectTagsDialog(
                context = this,
                tags = allTags,
                alreadySelectTags = getTagsFromRec(),
                onChangeSelectTags = onChangeSelectTags
            ).show()
        }

        val onResultAddMoreDialog = object: PlainTextDialog.OnResultEditTextDialog{
            override fun onAdd(text: String) {
                (rec_create_edit_more.adapter!! as MoreAdapter).addData(text)
            }

            override fun onCancel() {}
        }

        text_add_more.setOnClickListener {
            DialogFactory.createPlainTextDialog(
                this, DialogFactory.TitleVariant.INPUT_MORE,
                DialogFactory.HintVariant.MORE, onResultAddMoreDialog
            ).show()
        }

        save_button.setOnClickListener {
            val title = title_edit.text.toString()
            val desc = desc_edit.text.toString()
            val more = (rec_create_edit_more.adapter!! as MoreAdapter).getData()
            val tagsLesson = getTagsFromRec()

            val modelLesson = ModelLesson(title, desc, null, tagsLesson, more.ifEmpty {null}, idLesson)

            if (validateSaveData(modelLesson)){
                ConnectControllerAcceptPoint.connectController.saveModelLesson(modelLesson, object: ConnectController.OnGetData<ModelLesson>{
                    override fun onGet(data: ModelLesson) {
                        setResultLessonId(data.id!!)
                    }

                    override fun onFail(message: String) {
                        DialogFactory.createMessageDialogVariantTitle(
                            this@CreateEditLessonActivity, DialogFactory.TitleVariant.ERROR_SAVE,
                            message, DialogFactory.ButtonsVariant.OK_NULL
                        ).show()
                    }
                })
            }
        }

        getTags()
    }

    fun getTagsFromRec(): MutableList<String> = (rec_tags_create_edit_lesson.adapter!! as TagsAdapterSelectTag).tags

    fun setResultLessonId(id: Int){
        val intent = Intent()
        intent.putExtra("idLesson", id)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun getTags(){
        ConnectControllerAcceptPoint.connectController.requestGetAllLessons(
            object: ConnectController.OnGetData<List<ModelLesson>>{
                override fun onGet(data: List<ModelLesson>) {
                    allTags.clear()
                    allTags.addAll(Utils.convertLessonsToTags(data))
                }

                override fun onFail(message: String) {
                    DialogFactory.createMessageDialogVariantTitle(
                        this@CreateEditLessonActivity, DialogFactory.TitleVariant.ERROR_REQUEST_LESSONS,
                        message, DialogFactory.ButtonsVariant.OK_NULL
                    ).show()
                }
        })
    }

    private fun getData(id: String){
        ConnectControllerAcceptPoint.connectController.requestGetLesson(id, object: ConnectController.OnGetData<ModelLesson>{
            override fun onGet(data: ModelLesson) {
                setData(data)
            }

            override fun onFail(message: String) {
                DialogFactory.createMessageDialogVariantTitle(this@CreateEditLessonActivity,
                    DialogFactory.TitleVariant.ERROR_REQUEST_LESSON,
                    message,
                    DialogFactory.ButtonsVariant.OK_NULL
                ).show()
            }
        })
    }

    fun setData(modelLesson: ModelLesson){
        title_edit.setText(modelLesson.title)
        desc_edit.setText(modelLesson.text)
        if (modelLesson.moreList != null)
            (rec_create_edit_more.adapter!! as MoreAdapter).setData(modelLesson.moreList)
        (rec_tags_create_edit_lesson.adapter!! as TagsAdapterSelectTag).setData(modelLesson.tagsList)
    }

    private fun validateSaveData(modelLesson: ModelLesson): Boolean{
        if (modelLesson.title.isEmpty()){
            DialogFactory.createMessageDialogVariant(this,
                DialogFactory.TitleVariant.ERROR_EDITTEXT,
                DialogFactory.MessageVariant.ERROR_TITLE,
                DialogFactory.ButtonsVariant.OK_NULL
            ).show()
            return false
        }
        if (modelLesson.text.isEmpty()){
            DialogFactory.createMessageDialogVariant(this,
                DialogFactory.TitleVariant.ERROR_EDITTEXT,
                DialogFactory.MessageVariant.ERROR_DESC,
                DialogFactory.ButtonsVariant.OK_NULL
            ).show()
            return false
        }
        if (modelLesson.tagsList.isEmpty()){
            DialogFactory.createMessageDialogVariant(this,
                DialogFactory.TitleVariant.ERROR_EDITTEXT,
                DialogFactory.MessageVariant.ERROR_SELECT_ONE_TAG,
                DialogFactory.ButtonsVariant.OK_NULL
            ).show()
            return false
        }
        return true
    }
}