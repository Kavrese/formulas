package com.example.formulas.screens

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.formulas.R
import com.example.formulas.common.ConnectController
import com.example.formulas.common.ConnectControllerAcceptPoint
import com.example.formulas.common.UserDataSession
import com.example.formulas.common.Utils
import com.example.formulas.common.adapters.MoreAdapter
import com.example.formulas.common.adapters.TagsAdapterSelectTag
import com.example.formulas.common.dialogs.DialogFactory
import com.example.formulas.models.ModelLesson
import com.example.formulas.view.FavoriteStateButton
import com.example.formulas.view.PublishView
import kotlinx.android.synthetic.main.activity_lesson.*

class LessonActivity : AppCompatActivity() {

    var id: String? = null
    lateinit var lesson: ModelLesson

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lesson)

        id = intent.extras!!.getString("id")

        back.setOnClickListener {
            finish()
        }

        rec_more.apply {
            adapter = MoreAdapter(variantItem = MoreAdapter.VariantItem.SCREEN)
            layoutManager = LinearLayoutManager(this@LessonActivity)
        }

        rec_tags_lesson.apply {
            adapter = TagsAdapterSelectTag(
                variantSelect = TagsAdapterSelectTag.SelectTagsController.VariantSelect.NONE,
                variantItem = TagsAdapterSelectTag.VariantItem.SCREEN
            )
            layoutManager = Utils.createFlexBoxManager(this@LessonActivity)
        }

        favorite_button.onChangeState = object: FavoriteStateButton.OnChangeState{
            override fun onChange(state: FavoriteStateButton.State, isUser: Boolean) {
                if (isUser)
                    UserDataSession.userSession.saveStateFavoriteIdLesson(id!!.toInt(), state)
            }
        }

        publish_view.setViewVariantLayout(PublishView.VariantView.EDIT_PUBLISH_YOU)
        publish_view.onClickButton = object: PublishView.OnClickButton  {
            override fun onClick(variantButton: PublishView.VariantButton) {
                when (variantButton){
                    PublishView.VariantButton.EDIT -> {
                        toEditScreen()
                    }
                }
            }
        }

        if (id != null)
            getData()
        else
            DialogFactory.createMessageDialogVariant(this@LessonActivity,
                DialogFactory.TitleVariant.ERROR_REQUEST_LESSON,
                DialogFactory.MessageVariant.ERROR_GET_ID_LESSON,
                DialogFactory.ButtonsVariant.OK_NULL
            ).show()
    }

    private fun getData(){
        ConnectControllerAcceptPoint.connectController.requestGetLesson(id!!, object:
            ConnectController.OnGetData<ModelLesson> {
            override fun onGet(data: ModelLesson) {
                setDate(data)
            }

            override fun onFail(message: String) {
                DialogFactory.createMessageDialogVariantTitle(this@LessonActivity,
                    DialogFactory.TitleVariant.ERROR_REQUEST_LESSON,
                    message, DialogFactory.ButtonsVariant.OK_NULL
                ).show()
            }
        })
    }

    private fun setDate(lesson: ModelLesson){
        this.lesson = lesson
        title_lesson_screen.text = lesson.title
        desc_lesson_screen.text = lesson.text
        if (lesson.moreList == null){
            lin_more.visibility = View.GONE
        }else{
            lin_more.visibility = View.VISIBLE
            (rec_more.adapter!! as MoreAdapter).setData(lesson.moreList)
        }
        (rec_tags_lesson.adapter!! as TagsAdapterSelectTag).setData(lesson.tagsList)
        setIsFavoriteLesson()
    }

    private fun setIsFavoriteLesson(){
        val isFavorite = UserDataSession.userSession.favoriteUser!!.isExistObject(id!!.toInt())
        favorite_button.setState(
            if (isFavorite)
                FavoriteStateButton.State.SELECT
            else
                FavoriteStateButton.State.NO_SELECT)
    }

    private fun toEditScreen(){
        val intent = Intent(this, CreateEditLessonActivity::class.java)
        intent.putExtra("isCreateNew", false)
        intent.putExtra("idLesson", id!!.toInt())
        startActivityForResult(intent, 101)
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 101 && resultCode == Activity.RESULT_OK){
            getData()
            sendLessonEdit()
        }
    }

    private fun sendLessonEdit(){
        setResult(Activity.RESULT_OK)
    }
}