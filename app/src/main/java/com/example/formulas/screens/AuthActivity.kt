package com.example.formulas.screens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.formulas.R
import com.example.formulas.common.UserDataSession
import com.example.formulas.common.dialogs.DialogFactory
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        auth.setOnClickListener {
            val nickname = nickname.text.toString()
            if (!validateNickname(nickname)){
                DialogFactory.createMessageDialogVariant(this,
                    DialogFactory.TitleVariant.ERROR_EDITTEXT,
                    DialogFactory.MessageVariant.ERROR_NICKNAME_EDITTEXT,
                    DialogFactory.ButtonsVariant.OK_NULL).show()
                return@setOnClickListener
            }
            UserDataSession.userSession.enterUser(applicationContext, nickname)
            toMainActivity()
        }
    }

    private fun toMainActivity(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun validateNickname(nickname: String): Boolean{
        return nickname.isNotEmpty()
    }
}