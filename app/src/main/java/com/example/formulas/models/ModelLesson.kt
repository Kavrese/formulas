package com.example.formulas.models

data class ModelLesson(
    val title: String,
    val text: String,
    val idAuthor: Int? = null,
    val tagsList: List<String>,
    val moreList: List<String>?,
    var id: Int? = null
)
