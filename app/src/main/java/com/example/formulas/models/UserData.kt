package com.example.formulas.models

data class UserData(
    val nickname: String,
    val avatar: String? = null
)
